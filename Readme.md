# Projeto de Pizza de Menu

## 05/06/2019 
- [x] Criar repositório no GitHub
- [x] Criar Solution Visual Studio
- [X] Pensar na estrutura das Models
- [x] Versionar
- [x] Ler sobre arquitetura DDD

## 08/06/2019

- [x] Criar camadas conforme curso udemy

![Arquitetura inicial](img/arquiterura.png)
- [x] Criar migrations e database
- [X] Interface IService e IPizza
- [x] CRUD Pizza
  - [x] Index
  - [x] Details
  - [x] Create
  - [x] Edit
  - [x] Delete 
- [] Validar inputs de dados CRUD Pizza
  

- [] Validar campos e inputs dos CRUDS
  - [] Pizza
  - [] Ingredientes

## Atividades Futuras

- Validar compos;
- Refatorar o que precisar;
- Converter ingredientes em objetos;

