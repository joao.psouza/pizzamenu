﻿using Microsoft.AspNetCore.Mvc;
using PizzaMenu.Models.Entitites;
using PizzaMenu.Models.Services;
using System.Threading.Tasks;

namespace PizzaMenu.Controllers
{
    public class MenuController : Controller
    {
        private readonly MenuService _menuService;

        public MenuController(MenuService menuService)
        {
            _menuService = menuService;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _menuService.FindAllAsync());
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var result = await _menuService.FindByIdAsync(id);

            if (result == null)
            {
                return NotFound();
            }

            return View(result);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Pizza pizza)
        {
            if (pizza != null)
            {
                await _menuService.InsertAsync(pizza);
            }

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            else
            {
                return View(await _menuService.FindByIdAsync(id));

            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Pizza pizza)
        {
            await _menuService.UpdateAsync(pizza);

            return RedirectToAction(nameof(Index));
        }


        public async Task<IActionResult> Delete(int? id)
        {

            return View(await _menuService.FindByIdAsync(id));
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            await _menuService.RemoveAsync(id);
            return RedirectToAction(nameof(Index));
        }
    }
}