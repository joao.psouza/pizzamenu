﻿using Microsoft.EntityFrameworkCore;
using PizzaMenu.Models.Entitites;

namespace PizzaMenu.Models.Data
{
    public class PizzaContext : DbContext
    {
        public PizzaContext(DbContextOptions<PizzaContext> options)
            : base(options)
        {

        }

        public DbSet<Pizza> Pizza { get; set; }
    }
}
