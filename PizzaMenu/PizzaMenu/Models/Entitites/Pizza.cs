﻿using PizzaMenu.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PizzaMenu.Models.Entitites
{
    public class Pizza : IPizza
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Ingredients { get; set; }

        public Pizza()
        {

        }

        public Pizza(int id, string name, string ingredients)
        {
            Id = id;
            Name = name;
            Ingredients = ingredients;
        }
    }
}
