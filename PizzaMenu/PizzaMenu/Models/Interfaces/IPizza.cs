﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PizzaMenu.Models.Interfaces
{
    interface IPizza
    {
        int Id { get; set; }
        string Name { get; set; }
        string Ingredients { get; set; }
    }
}
