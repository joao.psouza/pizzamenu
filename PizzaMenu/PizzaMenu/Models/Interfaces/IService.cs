﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PizzaMenu.Models.Interfaces
{
    public interface IService<T> where T : class
    {
        Task<IEnumerable<T>> FindAllAsync();
        Task<T> FindByIdAsync(int? id);
        Task InsertAsync(T obj);
        Task UpdateAsync(T obj);

        Task RemoveAsync(int id);
    }
}
