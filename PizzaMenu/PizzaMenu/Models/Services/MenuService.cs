﻿using Microsoft.EntityFrameworkCore;
using PizzaMenu.Models.Data;
using PizzaMenu.Models.Entitites;
using PizzaMenu.Models.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PizzaMenu.Models.Services
{
    public class MenuService : IService<Pizza>
    {
        private readonly PizzaContext _context;

        public MenuService(PizzaContext context)
        {
            _context = context;
        }

        public async Task InsertAsync(Pizza obj)
        {
            await _context.AddAsync(obj);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Pizza>> FindAllAsync()
        {
            return await _context.Pizza.ToListAsync();
        }

        public async Task<Pizza> FindByIdAsync(int? id)
        {

            return await _context.Pizza.FirstOrDefaultAsync(p => p.Id == id);

        }

        public async Task UpdateAsync(Pizza obj)
        {
            _context.Update(obj);
            await _context.SaveChangesAsync();
        }

        public async Task RemoveAsync(int id)
        {

            var obj = await _context.Pizza.FindAsync(id);
            _context.Pizza.Remove(obj);
            await _context.SaveChangesAsync();

        }
    }
}
